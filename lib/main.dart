import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase/Screens/home_screen.dart';
import 'package:flutter_firebase/Screens/signin_screen.dart';

import 'firebase_options.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FirebaseAuth.instance
      .authStateChanges()
      .listen((User? user) {
    if (user == null) {
      runApp(

          MaterialApp(
            debugShowCheckedModeBanner: false,
            home: SigninScreen(),
          ));
    } else {
      runApp(

          MaterialApp(
            debugShowCheckedModeBanner: false,
            home: HomeScreen(),
          ));
    }
  });

}


