import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase/Screens/home_screen.dart';
import 'package:flutter_firebase/Screens/signin_screen.dart';
class OtpScreen extends StatefulWidget {
  const OtpScreen({Key? key}) : super(key: key);

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    var code = "";
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
                  alignment: Alignment.centerLeft,
                  child: Text("OTP")),
              Container(
                height: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(width: 1,color: Colors.grey)
                ),
                child:  TextField(
                  onChanged: (value) {
                    code = value;
                  },
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(15),
                    border: InputBorder.none,
                    hintText: "Enter your Code",
                  ),
                ),
              ),

              SizedBox(height: 50,),
              SizedBox(
                width: double.infinity,
                height: 55,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.lightBlue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                    onPressed: () async {
                      try{
                        PhoneAuthCredential credential = PhoneAuthProvider.credential(verificationId: SigninScreen.veryfy, smsCode: code);
                        await auth.signInWithCredential(credential);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen(),));
                      }catch(e){
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text("Ma otp khong dung")));

                    }

                    },
                    child: const Text("Confirm OTP")),
              )
            ],
          ),
        ),
      ),
    );
  }
}
